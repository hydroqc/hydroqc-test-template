1 - Create a docker-compose.yaml file based on the one in this folder

2 - Register and configure the runner
```
docker-compose up -d
docker-compose exec gitlab-runner gitlab-runner register
```
That last commande will start an interactive configuration, here are the important values to input:

    Enter the GitLab instance URL (for example, https://gitlab.com/): `https://gitlab.com`
    Enter the registration token: `Your project's CI/CD registration token`
    Enter tags for the runner (comma-separated): `hydroqc`
    Enter an executor: `docker`
    Enter the default Docker image (for example, ruby:2.7): ubuntu:22.04

You should now see your runner in your project!

The full runner documentation from gitlab can be found here https://docs.gitlab.com/runner/install/docker.html
