# hydroqc-test-template

[Français SVP!](./README.fr.md)

This repo is meant to help us test the hydroqc library with different accounts without having you give us your account credentials. The way it works is you create a new blank repo under your account and configure your HQ account information in the private variables of your repo's CI/CD configuration. We then configure our CI/CD pipeline to trigger yours and gather the results to check the validity of the code.

We only accept new tests for scenarios that are not already covered in our scenarios table [here](https://gitlab.com/hydroqc/hydroqc/-/blob/main/README.md).

**We are still working out the details of some of the steps below, thanks for your patience**

What you need:
- Some IT/computer skills
- Working knowledge in Gitlab or at least be willing to figure it out
- You will need to operate your own runner. The easiest is probably to use the docker based installation. We have a basic installation guide [here](https://gitlab.com/hydroqc/hydroqc-test-template/-/blob/main/runner/README.md)

## Configuration

When creating your repo make sure to chose a name that make sense to you (mdallaire1-hydroqc-test).

1. Create a blank repo under your own account.
    - https://gitlab.com/projects/new#blank_project
    - Name: ex: hydroqc-test
    - Slug: ex: hydroqc-test
    - Visibility: Private
    - Uncheck "Initialize repository with a README"

2. Add the hydroqc service account to your repo
   - Under "Manage -> Members" click "Invite members"
   - Type "hydroqc_sa" then click the account named "HydroQC Tests"
   - Select the "Developper" role
   - Click invite

3. Add your account information to the CI/CD variables
    - Under "Settings -> CI/CD -> Variables" create the following variables:
      | Key | Value | Protected | Masked | Description |
      | - | - | - | - | - |
      | HYDROQC_0_USERNAME | email@domain.tld | Yes | Yes | The email address you use when login to HQ web portal |
      | HYDROQC_0_PASSWORD | Soleil123 | Yes | Yes | The password for the web portal |
      | HYDROQC_0_CUSTOMER_NUMBER | 0123456789| Yes | No | The customer number for your account |
      | HYDROQC_0_EPP_ENABLED | `true` or `false` | Yes | No | Are you subscribed to the Equal Payment Plan? |
      | HYDROQC_0_RATE | D/DPC/DT/M | Yes | No | What is the rate for this account|
      | HYDROQC_0_RATE_OPTION | NONE/CPC/Others | Yes | No | Rate option if any.|
      | RUNNER_TAG | hydroqc | Yes | No | Tag set on your runner |

      If you need to test more than one account you can add information for the second one by incrementing the number between underscores.

4. Configure your runner for your project.
    - How to do this for various platform here: https://docs.gitlab.com/runner/install/
    - You need to use a docker or kubernetes executor
    - You will need to tag it with the "hydroqc" tag
    - Do not configure it as a "protected runner" otherwise it won't work.
    - Make sur to disable the gitlab shared runners under your project CI/CD configuration

**You will need to coordinate with us to provide you the key for the following steps. Please [open an issue](https://gitlab.com/hydroqc/hydroqc-test-template/-/issues) and provide the necessary details then reach out to us on discord [#development](https://discord.gg/NWnfdfRZ7T) channel so we can help you.**

5. Add the hydroqc project Deploy Key to your repo (that the team created by going under "Settings -> Repository -> Mirroring" and creating a new push mirroring account with ssh auth, we also made sure to import the host key)

    - Under "Settings -> Repository -> Deploy keys" add the following key to authorize the hydroqc project to update the gitlab CI file.

6. Change protected branch configuration "Settings -> Repository -> Protected Branch" to "Developper + Maintainer" and activate "Force push"



## FAQ

- Is this safe, do you see my credentials?
    - We do not see your credentials since they are under your own repo, the CI/CD job also runs under your own account. You do need to trust us to not alter the CI/CD job to dump your credentials. We can assure that the whole reason for us to create this CI/CD process is to stop people sending us their account information when they have issues. In the end the decision to perform theses steps is yours and you accept the risk.

- Why won't you add my account to the test?
    - We don't want to test everyone's account but we want to be able to test various combination of account configuration. See the table below to see our coverage of account types and if your particular account could help us increase our test coverage.

- What if I change my account password for Hydro-Quebec's website?
    - Please update the variables under your CI/CD configuration to ensure the tests continue working.

- Why did you remove my account from the tests?
    - Did your account login info change? Did you close your account? Did your account configuration change (activated EPP for example)? When changes occur we need you to change the variables under your CI/CD configuration for our tests to keep working. When things like that fail we will try to reach you through gitlab to ask that you fix the issue but if you don't we will eventually stop requesting tests for your account.


