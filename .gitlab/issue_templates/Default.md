# I want my test to be added to the private tests.

## What is your account scenario?

Check what applies to your account

* [ ] EPP? (Equal Payment Plan/Mode versement égaux)
* [ ] Winter Credit?
* [ ] More than one contract?
* [ ] More than one customer?
* [ ] Other / not listed (Please elaborate below)

Other comments or relevant details:

## Private test and runner configuration

* [ ] I have followed the steps from the readme
* [ ] I have a runner for the project and it is tagged with hydroqc

The gitlab project ID for my test is : 


